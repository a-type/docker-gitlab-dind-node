FROM docker:stable-git

# Google Cloud SDK
ENV GCLOUD_SDK_URL="https://dl.google.com/dl/cloudsdk/release/google-cloud-sdk.tar.gz" \
    PATH="/opt/google-cloud-sdk/bin:${PATH}" \
    HELM_VERSION="v2.9.1"

WORKDIR /opt/app

RUN apk --update --no-cache add \
        bash \
        ca-certificates \
        curl \
        openssl \
        python && \
    wget -O - -q "${GCLOUD_SDK_URL}" | tar zxf - -C /opt && \
    ln -s /lib /lib64 && \
    gcloud config set core/disable_usage_reporting true && \
    gcloud config set component_manager/disable_update_check true && \
    gcloud config set metrics/environment github_docker_image && \
    gcloud --version && \
    rm -rf /tmp/* && rm -rf /opt/google-cloud-sdk/.install/.backup

RUN gcloud components install kubectl

# Install Helm
ENV FILENAME helm-${HELM_VERSION}-linux-amd64.tar.gz
ENV HELM_URL https://storage.googleapis.com/kubernetes-helm/${FILENAME}

RUN curl -o /tmp/$FILENAME ${HELM_URL} \
  && tar -zxvf /tmp/${FILENAME} -C /tmp \
  && mv /tmp/linux-amd64/helm /bin/helm \
  && rm -rf /tmp/${FILENAME} \
  && rm -rf /tmp/linux-amd64

# NodeJS
RUN apk update

RUN apk add --update nodejs nodejs-npm

RUN node -v

RUN npm -v

RUN npm i -g lerna
